var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var fs = require('fs');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.post('/upload', (request, response) => {
  let buffer = new Buffer([])
  request.on('data', function (foo) {
    buffer = Buffer.concat([buffer, foo]);
  });
  request.on('end', function () {
    var str = buffer.toString();
    imageType = isImage(buffer.toString());
    var index = str.indexOf(imageType) + (imageType + "\r\n\r\n").length;
    var indexBoundayToBuffer = str.lastIndexOf('------WebKitFormBoundary') + (buffer.length - str.length);
    var newBuffer = Buffer.alloc(buffer.length);
    buffer.copy(newBuffer, 0, index, indexBoundayToBuffer);
    var type = imageType.substr("image/".length);
    fs.writeFile("./image." + type, newBuffer, function (err, ok) {
      if (err) {
        return false;
      }
      response.send('File Uploaded');
    });
  });
});
app.get('/', (request, response, next) => {
  response.render('index', { title: 'File Upload'})
});

function isImage(bufferString) {
  if (bufferString.indexOf('image/png') != -1) {
    return 'image/png'
  }else if (bufferString.indexOf('image/jpeg') != -1) {
    return 'image/jpeg'
  } else if (bufferString.indexOf('image/bmp' != -1)) {
    return 'image/bmp'
  } else if (bufferString.indexOf('image/gif' != -1)) {
    return 'image/gif'
  }
  else false
}
// catch 404 and forward to error handler
app.use(function(request, response, next) {
  next(createError(404));
});

// error handler
app.use(function(err, request, response, next) {
  // set locals, only providing error in development
  response.locals.message = err.message;
  response.locals.error = request.app.get('env') === 'development' ? err : {};

  // render the error page
  response.status(err.status || 500);
  response.render('error');
});

module.exports = app;
